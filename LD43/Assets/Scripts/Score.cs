﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public float score;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene().name == "cut3")
        {
            FindObjectOfType<Text>().text = score.ToString("0.00") + "%";
        }
    }

    public void SetScore(float hp, float condition)
    {
        score = (hp + condition) / 2f;
    }
}
