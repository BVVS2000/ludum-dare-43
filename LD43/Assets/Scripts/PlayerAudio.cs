﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlayerAudio : MonoBehaviour {

    public float stepOffset = 0.5f;
    public AudioClip stepSound;
    public AudioClip hitSound;
    public AudioSource stepAudioSource;
    public AudioSource hitAudioSource;
    public Movement movement;

    private float toPlayStep = 0.0f;

    private void Start()
    {
        stepAudioSource.clip = stepSound;
        hitAudioSource.clip = hitSound;
    }

    private void Update()
    {
        if(toPlayStep>0.0f)
        {
            toPlayStep -= Time.deltaTime;
        }

        if(movement.isMoving)
        {
            if(toPlayStep<=0.0f)
            {
                stepAudioSource.Play();
                toPlayStep = stepOffset;
            }
        }
    }

    public void Hit()
    {
        hitAudioSource.Play();
    }
}
