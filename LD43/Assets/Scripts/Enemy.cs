﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]
public class Enemy : MonoBehaviour {

    public float hp = 100f;
    public float maxHP = 100f;

    public float rangeOfIntrest = 10f;
    public float minDistance = 2f;
    public float speed;

    Transform target;
    Rigidbody2D rb;
    WolfAtack wAtack;

    public Animator animator;
    public Slider hpSlider;

    bool intrested = false;

    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        rb = GetComponent<Rigidbody2D>();
        wAtack = GetComponent<WolfAtack>();
        hp = maxHP;
    }

    public void GetDamage(float val)
    {
        hp -= val;

        if (hp <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        hpSlider.value = hp / maxHP;

        if (hpSlider.value == 1)
        {
            hpSlider.transform.GetChild(0).gameObject.SetActive(false);

        }
        else
        {
            hpSlider.transform.GetChild(0).gameObject.SetActive(true);
        }

        if (!intrested)
        {
            if (Vector2.Distance(transform.position, target.position) <= rangeOfIntrest)
            {
                intrested = true;
            }
        }
        else
        {
            if (wAtack)
            {
                if (Vector2.Distance(transform.position, target.position) > minDistance && !wAtack.isAtacking)
                {
                    rb.MovePosition(transform.position + (target.transform.position - transform.position).normalized * speed * Time.deltaTime);
                    animator.SetBool("isMoving", true);
                }
                else
                {
                    animator.SetBool("isMoving", false);
                }
            }
        }
    }
}
