﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class BossCutscene : MonoBehaviour {

    public Transform camera;
    public float cameraSpeed;
    public float stopTime;
    float onPhase = 0.0f;
    float bossPosition;
    bool played = false;

    Rulles rulles;
    Transform player;
    bool cutscene = false;
    int phase = 1;

    public AudioSource source;
    public AudioClip clip;

    private void Start()
    {
        rulles = FindObjectOfType<Rulles>();
        bossPosition = camera.GetComponent<CameraFollow>().maxX;
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        if (cutscene)
        {
            camera.GetComponent<CameraFollow>().enabled = false;
            switch (phase)
            {
                case 1:
                    {
                        if (camera.position.x < camera.GetComponent<CameraFollow>().maxX)
                        {
                            camera.Translate(Vector2.right * cameraSpeed * Time.deltaTime);

                            if (camera.position.x > camera.GetComponent<CameraFollow>().maxX)
                            {
                                camera.position = new Vector3(camera.GetComponent<CameraFollow>().maxX, camera.position.y, camera.position.z);
                                GetComponent<AudioSource>().Play();
                                phase++;
                            }
                        }

                        break;
                    }
                case 2:
                    {   
                        onPhase += Time.deltaTime;
                        if(onPhase >= stopTime)
                        {
                            phase++;
                        }
                        break;
                    }
                case 3:
                    {
                        if (camera.position.x > player.position.x)
                        {
                            camera.Translate(Vector2.left * cameraSpeed * Time.deltaTime);

                            if (camera.position.x < player.position.x)
                            {
                                camera.position = new Vector3(player.position.x, camera.position.y, camera.position.z);
                                phase++;
                            }
                        }

                        break;
                    }
                case 4:
                    {
                        rulles.bossCanDoAnything = rulles.canMove = rulles.canAtack = rulles.canUseSkills = true;
                        cutscene = false;

                        break;
                    }
            }
        }
        else
        {
            camera.GetComponent<CameraFollow>().enabled = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            rulles.canMove = rulles.canAtack = rulles.canUseSkills = false;
            cutscene = true;
            source.clip = clip;
            source.Play();
        }
    }
}
