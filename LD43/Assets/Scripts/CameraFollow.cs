﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private float speed = 10.0f;

    private Transform follow;

    public float minX;
    public float maxX;

    private void Start()
    {
        follow = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void FixedUpdate()
    {
        transform.position += (Vector3)((Vector2)(follow.position - transform.position)).normalized * Vector2.Distance(transform.position, follow.position) * speed * Time.deltaTime;
        transform.position = new Vector3(transform.position.x, 0, transform.position.z);

        if(transform.position.x < minX)
        {
            transform.position = new Vector3(minX, 0, transform.position.z);
        }
        else if (transform.position.x > maxX)
        {
            transform.position = new Vector3(maxX, 0, transform.position.z);
        }
    }
}
