﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class WeaponManager : MonoBehaviour
{
    public List<WeaponSlot> weapons = new List<WeaponSlot>();

    public GameObject hand;
    public GameObject actWeapon;

    public Image weaponImg;

    int choosed = 0;

    public bool hasWeapon
    {
        get
        {
            return weapons.Count > 0;
        }
    }

    private void Start()
    {
        if (hasWeapon)
        {
            actWeapon = Instantiate(weapons[choosed].gameObject, transform);
            hand.SetActive(false);
            if (FindObjectOfType<FlipXPlayer>().fliped)
            {
                FindObjectOfType<WeaponFlip>().Flip();
            }
        }
    }

    private void Update()
    {
        if (hasWeapon)
        {
            hand.SetActive(false);
            weaponImg.sprite = weapons[choosed].icon;
            weaponImg.gameObject.SetActive(true);
        }
        else
        {
            hand.SetActive(true);
            weaponImg.gameObject.SetActive(false);
        }

        if (Input.mouseScrollDelta.y != 0 && weapons.Count > 1)
        {
            if (Input.mouseScrollDelta.y > 0)
            {
                choosed++;

                if (choosed >= weapons.Count)
                {
                    choosed = 0;
                }
            }
            else
            {
                choosed--;

                if (choosed < 0)
                {
                    choosed = weapons.Count - 1;
                }
            }

            if (actWeapon)
            {
                Destroy(actWeapon);
            }

            actWeapon = Instantiate(weapons[choosed].weapon.gameObject, transform);
            if (FindObjectOfType<FlipXPlayer>().fliped)
            {
                FindObjectOfType<WeaponFlip>().Flip();
            }
        }
    }

    public bool ContainsWeapon(string name)
    {
        foreach(WeaponSlot a in weapons)
        {
            if(a.weapon.Name == name)
            {
                return true;
            }
        }

        return false;
    }

    public void AddWeapon(WeaponSlot wep)
    {
        weapons.Add(wep);

        if (weapons.Count == 1)
        {
            actWeapon = Instantiate(weapons[choosed].weapon.gameObject, transform);
            if (FindObjectOfType<FlipXPlayer>().fliped)
            {
                FindObjectOfType<WeaponFlip>().Flip();
            }
        }
    }
}
