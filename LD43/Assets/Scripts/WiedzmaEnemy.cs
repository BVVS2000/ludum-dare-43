﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody2D))]
public class WiedzmaEnemy : MonoBehaviour
{

    public float hp = 100f;
    public float maxHP = 100f;

    public float rangeOfIntrest = 10f;
    public float minDistance = 2f;
    public float speed;

    public Slider hpSlider;

    Transform target;
    Rigidbody2D rb;
    WiedzmaAtack wAtack;
    Rulles rulles;

    Slider bossHP;

    bool intrested = false;

    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        rb = GetComponent<Rigidbody2D>();
        wAtack = GetComponent<WiedzmaAtack>();
        rulles = FindObjectOfType<Rulles>();
        bossHP = GameObject.FindGameObjectWithTag("BossHP").GetComponent<Slider>();
        hp = maxHP;
    }

    public void GetDamage(float val)
    {
        hp -= val;

        if(hp<=0)
        {
            if(gameObject.tag == "boss")
            {
                SceneManager.LoadScene("cut2");
            }

            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if (gameObject.tag == "boss" && rulles.bossCanDoAnything)
        {
            bossHP.transform.GetChild(0).gameObject.SetActive(true);
            bossHP.transform.GetChild(1).gameObject.SetActive(true);
            bossHP.value = hp / maxHP;
            if (hpSlider)
            {
                hpSlider.transform.GetChild(0).gameObject.SetActive(false);
            }
        }
        else if (gameObject.tag == "Enemy")
        {
            bossHP.transform.GetChild(0).gameObject.SetActive(false);
            bossHP.transform.GetChild(1).gameObject.SetActive(false);

            if (hpSlider) 
            {
                hpSlider.value = hp / maxHP;

                if (hpSlider.value == 1)
                {
                    hpSlider.transform.GetChild(0).gameObject.SetActive(false);

                }
                else
                {
                    hpSlider.transform.GetChild(0).gameObject.SetActive(true);
                }
            }
        }
        else
        {
            bossHP.transform.GetChild(0).gameObject.SetActive(false);
            bossHP.transform.GetChild(1).gameObject.SetActive(false);
            if (hpSlider)
            {
                hpSlider.transform.GetChild(0).gameObject.SetActive(false);
            }
        }

        if (!intrested && (gameObject.tag == "Enemy" || (gameObject.tag == "boss" && rulles.bossCanDoAnything)))
        {
            if (Vector2.Distance(transform.position, target.position) <= rangeOfIntrest)
            {
                intrested = true;
            }
        }
        else
        {
            if (wAtack && (gameObject.tag=="Enemy" || (gameObject.tag=="boss" && rulles.bossCanDoAnything)))
            {
                if (Vector2.Distance(transform.position, target.position) > minDistance && !wAtack.isAtacking)
                {
                    rb.MovePosition(transform.position + (target.transform.position - transform.position).normalized * speed * Time.deltaTime);
                }
            }
        }
    }
}
