﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : Skill
{
    [SerializeField] private float dashTime = 1f;
    [SerializeField] private float dashSpeed = 10f;
    private float onDash = 0.0f;
    private bool isDashing = false;

    private Vector2 dashDirection;
    
    private Rigidbody2D rb;

    private void Start()
    {
        rb = transform.parent.parent.GetComponent<Rigidbody2D>();
    }

    private void LateUpdate()
    {
        if (isDashing)
        {
            onDash += Time.deltaTime;
            rb.MovePosition((Vector2)transform.parent.position + dashDirection * dashSpeed * Time.deltaTime);
            
            if (onDash > dashTime)
            {
                isDashing = false;
                StopDash();
            }
        }

        transform.parent.parent.GetComponentInChildren<SpriteRenderer>().GetComponent<Animator>().SetBool("isDashing", isDashing);
    }

    override public void Use()
    {
        transform.parent.parent.GetComponent<Movement>().enabled = false;
        onDash = 0.0f;
        isDashing = true;
        dashDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.parent.parent.position;
        dashDirection.Normalize();
    }

    public override bool Condition()
    {
        if (!isDashing)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void StopDash()
    {
        isDashing = false;
        transform.parent.parent.GetComponent<Movement>().enabled = true;
    }
}
