﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Skiller : MonoBehaviour {

    public List<Skill> skills = new List<Skill>();
    public List<Image> skillsIcons = new List<Image>();

    PlayerStats stats;
    Rulles rulles;

    public bool ContainsSkill(Skill skill)
    {
        foreach(Skill s in skills)
        {
            if(s.GetType() == skill.GetType())
            {
                return true;
            }
        }

        return false;
    }

	void Start ()
    {
		stats = FindObjectOfType<PlayerStats>();
        rulles = FindObjectOfType<Rulles>();
        RefreshSkills();
    }

	void Update ()
    {
        rulles = FindObjectOfType<Rulles>();
        if (rulles.canUseSkills)
        {
            if (Input.GetButtonDown("Skill 1"))
            {
                if (stats.condition >= skills[0].cost && skills[0].Condition())
                {
                    stats.condition -= skills[0].cost;
                    skills[0].Use();
                }
            }
            else if (Input.GetButtonDown("Skill 2"))
            {
                if (stats.condition >= skills[1].cost && skills[1].Condition())
                {
                    stats.condition -= skills[1].cost;
                    skills[1].Use();
                }
            }
            else if (Input.GetButtonDown("Skill 3"))
            {
                if (stats.condition >= skills[2].cost && skills[2].Condition())
                {
                    stats.condition -= skills[2].cost;
                    skills[2].Use();
                }
            }
            else if (Input.GetButtonDown("Skill 4"))
            {
                if (stats.condition >= skills[3].cost && skills[3].Condition())
                {
                    stats.condition -= skills[3].cost;
                    skills[3].Use();
                }
            }
        }

        foreach(Image i in skillsIcons)
        {
            if(i.sprite != null)
            {
                i.gameObject.SetActive(true);
            }
            else
            {
                i.gameObject.SetActive(false);
            }
        }
    }

    public void AddSkill(Sprite icon)
    {
        skillsIcons[skills.Count - 1].sprite = icon;
    }

    public void RefreshSkills()
    {
        skills = new List<Skill>();
        for (int i = 0; i < transform.childCount; i++)
        {
            skills.Add(transform.GetChild(i).GetComponent<Skill>());
        }
    }
}
