﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour {

    public float hp = 100f;
    public float condition = 100f;

    public Slider hpSlider;
    public Slider conditionSlider;

    PlayerAudio audio;

    private void Start()
    {
        audio = FindObjectOfType<PlayerAudio>();
    }

    private void Update()
    {
        hpSlider.value = hp / 100;
        conditionSlider.value = condition / 100;

        hpSlider.transform.GetComponentInChildren<Text>().text = hp.ToString() + "/100";
        conditionSlider.transform.GetComponentInChildren<Text>().text = condition.ToString() + "/100";
    }

    public void GetDamage(float val)
    {
        hp -= val;
        audio.Hit();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Ball"))
        {
            GetDamage(collision.GetComponent<Ball>().dmg);
            Destroy(collision.gameObject);
        }
    }
}
