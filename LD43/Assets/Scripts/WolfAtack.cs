﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfAtack : MonoBehaviour {

    public float prepareTime;
    public float armedTime;
    public float dmg;
    public float delay;
    public float atackRange;
    public float distanceToStartAtack;

    public bool isAtacking = false;
    public bool armed = false;

    private float toAtack = 0.0f;
    private float toEndPrepare = 0.0f;
    private float toEndBeArmed = 0.0f;

    Transform player;
    public Animator animator;
    public PlaySound sound;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        animator.SetBool("isAtacking", isAtacking);

        if(Vector2.Distance(player.position, transform.position) <= distanceToStartAtack && toAtack <= 0.0f && !isAtacking)
        {
            Atack();
        }
        else if(toAtack > 0.0f && !isAtacking)
        {
            toAtack -= Time.deltaTime;
        }

        if(isAtacking && !armed)
        {
            toEndPrepare -= Time.deltaTime;
            
            if(toEndPrepare<=0.0f)
            {
                armed = true;
                toEndBeArmed = armedTime;
            }
        }

        if(isAtacking && armed)
        {
            if (Vector2.Distance(player.position, transform.position) <= atackRange)
            {
                player.GetComponent<PlayerStats>().GetDamage(dmg);
                armed = isAtacking = false;
                sound.Play();
            }
            else
            {
                toEndBeArmed -= Time.deltaTime;
                if (toEndBeArmed <= 0.0f)
                {
                    armed = isAtacking = false;
                }
            }
        }
    }

    void Atack()
    {
        toEndPrepare = prepareTime;
        isAtacking = true;
        toAtack = delay;
    }
}
