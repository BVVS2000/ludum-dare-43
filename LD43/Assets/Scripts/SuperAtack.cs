﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperAtack : Skill
{
    WeaponManager manager;

    private void Start()
    {
        manager = FindObjectOfType<WeaponManager>();
    }

    public override void Use()
    {
        manager.actWeapon.GetComponent<Atack>().superatack = true;
    }

    public override bool Condition()
    {
        return !manager.actWeapon.GetComponent<Atack>().superatack;
    }
}
