﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwtichMenu : MonoBehaviour {

    public Image menu1, menu2;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(menu1.gameObject.activeSelf == true)
            {
                menu1.gameObject.SetActive(false);
                menu2.gameObject.SetActive(true);
            }
            else
            {
                menu1.gameObject.SetActive(true);
                menu2.gameObject.SetActive(false);
            }
        }
    }
}
