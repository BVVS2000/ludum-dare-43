﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

    Rulles rulles;
    PlayerStats stats;
    
    public Image gameOver;

    private void Start()
    {
        rulles = FindObjectOfType<Rulles>();
        stats = FindObjectOfType<PlayerStats>();
    }

    private void LateUpdate()
    {
        if(stats.hp <= 0.0f)
        {
            rulles.canMove = rulles.canAtack = rulles.bossCanDoAnything = rulles.canUseSkills = false;
            gameOver.gameObject.SetActive(true);
            Invoke("Retry", 5f);
        }
    }

    void Retry()
    {
        Destroy(stats.gameObject);
        Destroy(gameObject);
        SceneManager.LoadScene("Wioska");
    }
}
