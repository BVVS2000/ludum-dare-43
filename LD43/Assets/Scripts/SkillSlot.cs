﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SkillSlot : MonoBehaviour
{

    public Skill skill;
    public string skillName;
    public string description;
    public float cost;
    public Sprite icon;

    public bool highlighted = false;
    public bool hardHighlighted = false;

    public void Highlight()
    {
        SkillSlot s = CurrentHighlighted;
        if(s)
        {
            s.highlighted = false;
        }
        
        highlighted = true;
    }

    public void UnHighlight()
    {
        highlighted = false;
    }

    public void HardHighlight()
    {
        if (!hardHighlighted)
        {
            CurrentHardHighlighted.hardHighlighted = false;
            hardHighlighted = true;
        }
    }

    private SkillSlot CurrentHighlighted
    {
        get
        {
            return FindObjectsOfType<SkillSlot>().Where(s => s.highlighted).FirstOrDefault();
        }
    }

    private SkillSlot CurrentHardHighlighted
    {
        get
        {
            return FindObjectsOfType<SkillSlot>().Where(s => s.hardHighlighted).First();
        }
    }
}
