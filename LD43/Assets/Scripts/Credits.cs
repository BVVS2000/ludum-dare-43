﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credits : MonoBehaviour {

    public GameObject objToOff;
    public GameObject credits;

    bool isCredits;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            isCredits = false;
        }

        if(isCredits)
        {
            objToOff.SetActive(false);
            credits.SetActive(true);
        }
        else
        {
            objToOff.SetActive(true);
            credits.SetActive(false);
        }
    }

    public void ShowCredits()
    {
        isCredits = true;
    }
}
