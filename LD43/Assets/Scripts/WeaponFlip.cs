﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponFlip : MonoBehaviour {

    public SpriteRenderer sprite;
    public Transform pivot;
    public Animator anim;

    Vector2 defaultPos;

    public Vector2 newPos;

    private void Start()
    {
        defaultPos = pivot.localPosition;
    }

    private void Update()
    {
        if (Input.GetAxisRaw("Horizontal") == -1)
        {
            sprite.sortingOrder = -1;
            sprite.flipX = true;
            pivot.localPosition = newPos;
            anim.SetBool("Reverse", true);
        }
        else if (Input.GetAxisRaw("Horizontal") == 1)
        {
            sprite.sortingOrder = 1;
            sprite.flipX = false;
            pivot.localPosition = defaultPos;
            anim.SetBool("Reverse", false);
        }
    }


    public void Flip()
    {
        sprite.sortingOrder = -1;
        sprite.flipX = true;
        pivot.localPosition = newPos;
        anim.SetBool("Reverse", true);
    }
}
