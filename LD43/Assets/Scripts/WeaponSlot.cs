﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class WeaponSlot : MonoBehaviour
{

    public Atack weapon;
    public string weaponName;
    public float cost;
    public Sprite icon;

    public bool highlighted = false;
    public bool hardHighlighted = false;

    public void Highlight()
    {
        WeaponSlot s = CurrentHighlighted;
        if(s)
        {
            s.highlighted = false;
        }
        
        highlighted = true;
    }

    public void UnHighlight()
    {
        highlighted = false;
    }

    public void HardHighlight()
    {
        if (!hardHighlighted)
        {
            CurrentHardHighlighted.hardHighlighted = false;
            hardHighlighted = true;
        }
    }

    private WeaponSlot CurrentHighlighted
    {
        get
        {
            return FindObjectsOfType<WeaponSlot>().Where(s => s.highlighted).FirstOrDefault();
        }
    }

    private WeaponSlot CurrentHardHighlighted
    {
        get
        {
            return FindObjectsOfType<WeaponSlot>().Where(s => s.hardHighlighted).First();
        }
    }
}
