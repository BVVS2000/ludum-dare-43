﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Judgment : Skill
{
    [SerializeField] private float length = 3.0f;
    [SerializeField] private float dmgMultiplier = 1.2f;
    [SerializeField] private float dmgOffset = 0.5f;

    private float OnEmbeding = 0.0f;
    private float lastAtack = 0.0f;

    private bool isEmbeding = false;

    private Atack atack;

    private void Start()
    {
        atack = FindObjectOfType<Atack>();
    }

    private void Update()
    {
        if (isEmbeding)
        {
            OnEmbeding += Time.deltaTime;

            if (lastAtack <= 0.0f)
            {
                List<GameObject> allEnemies = new List<GameObject>(GameObject.FindGameObjectsWithTag("Enemy"));
                List<GameObject> enemies = new List<GameObject>();

                foreach (GameObject obj in allEnemies)
                {
                    if (Vector2.Distance(obj.transform.position, transform.position) <= atack.atackDistance * (atack.superatack ? 2 : 1))
                    {
                        enemies.Add(obj);
                    }
                }

                foreach (GameObject obj in enemies)
                {
                    obj.GetComponent<Enemy>().GetDamage(atack.dmg * dmgMultiplier * (atack.superatack ? 2 : 1));
                }

                lastAtack = dmgOffset;
            }
            else
            {
                lastAtack -= Time.deltaTime;
            }

            if(OnEmbeding >= length)
            {
                isEmbeding = false;
            }
        }
    }

    public override void Use()
    {
        isEmbeding = true;
        transform.parent.parent.GetComponentInChildren<Atack>().GetComponentInChildren<Animator>().SetTrigger("Kolowrotek");

    }

    public override bool Condition()
    {
        return !isEmbeding;
    }
}
