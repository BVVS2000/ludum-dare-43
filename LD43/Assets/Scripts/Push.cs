﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Push : Skill
{
    public float dmgMultiplier = 1.0f;
    public float distanceMultiplier = 1.5f;

    private Atack atack;

    private void Start()
    {
        atack = FindObjectOfType<Atack>();
    }

    public override void Use()
    {
        List<GameObject> allEnemies = new List<GameObject>(GameObject.FindGameObjectsWithTag("Enemy"));
        List<GameObject> tempEnemies = new List<GameObject>();
        List<GameObject> enemies = new List<GameObject>();

        foreach (GameObject obj in allEnemies)
        {
            if (Vector2.Distance(obj.transform.position, transform.position) <= atack.atackDistance * 1.5)
            {
                tempEnemies.Add(obj);
            }
        }

        foreach (GameObject obj in tempEnemies)
        {
            if (Angle(obj) >= 88 && Angle(obj) <= 92)
            {
                enemies.Add(obj);
            }
        }

        foreach (GameObject en in enemies)
        {
            en.GetComponent<Enemy>().GetDamage(atack.dmg * dmgMultiplier * (atack.superatack ? 2 : 1));
        }
    }

    public override bool Condition()
    {
        return true;
    }

    float Angle(GameObject obj)
    {
        Vector2 dir = obj.transform.position - GameObject.FindGameObjectWithTag("Player").transform.position;
        dir.Normalize();
        return Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
    }
}
