﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroy : MonoBehaviour {

    public List<GameObject> objects = new List<GameObject>();

    private void Start()
    {
        foreach(GameObject obj in objects)
        {
            DontDestroyOnLoad(obj);
        }
    }
}
