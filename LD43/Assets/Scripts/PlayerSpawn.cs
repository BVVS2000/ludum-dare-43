﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour {

    public Transform spawnPoint;

    private void Start()
    {
        GameObject.FindGameObjectWithTag("Player").transform.position = spawnPoint.position;
    }
}
