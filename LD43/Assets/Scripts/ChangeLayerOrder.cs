﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeLayerOrder : MonoBehaviour {

    [SerializeField] private Transform controlPoint;
    private Transform playerPos;

    private void Start()
    {
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        if(playerPos.position.y < controlPoint.position.y)
        {
            GetComponent<SpriteRenderer>().sortingOrder = -2;
        }
        else
        {
            GetComponent<SpriteRenderer>().sortingOrder = 2;
        }
    }
}
