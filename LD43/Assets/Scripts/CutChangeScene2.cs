﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class CutChangeScene2 : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine("waiter");

        FindObjectOfType<Score>().SetScore(FindObjectOfType<PlayerStats>().hp, FindObjectOfType<PlayerStats>().condition);

        GameObject.FindGameObjectWithTag("canvas").gameObject.SetActive(false);
    }

    IEnumerator waiter()
    {
        yield return new WaitForSeconds(62.15f);
        SceneManager.LoadScene("cut3", LoadSceneMode.Single);
    }
}
