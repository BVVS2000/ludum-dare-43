﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class HighlightedSkillDescription : MonoBehaviour {

    public Text title;
    public Text description;
    public Image icon;

    private SkillSlot CurrentHighlighted
    {
        get
        {
            return FindObjectsOfType<SkillSlot>().Where(s => s.highlighted).FirstOrDefault();
        }
    }

    private SkillSlot CurrentHardHighlighted
    {
        get
        {
            return FindObjectsOfType<SkillSlot>().Where(s => s.hardHighlighted).First();
        }
    }


    private void Update()
    {
        SkillSlot s = CurrentHighlighted;

        if(!s)
        {
            s = CurrentHardHighlighted;
        }

        title.text = s.skillName;
        description.text = "Description:\n" + s.description + "\n\nPrize: " + s.cost + " CP";
        icon.sprite = s.icon;
    }
}
