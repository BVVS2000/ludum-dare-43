﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class WeaponBuy : MonoBehaviour {

    PlayerStats stats;
    WeaponManager weps;
    public float rangeOfBuy = 1f;
    public Image interact;
    public Image menu;

    public PlaySound sound;

    private void Start()
    {
        stats = FindObjectOfType<PlayerStats>();
        weps = FindObjectOfType<WeaponManager>();
    }

    private WeaponSlot CurrentHardHighlighted
    {
        get
        {
            return FindObjectsOfType<WeaponSlot>().Where(s => s.hardHighlighted).First();
        }
    }

    public void BuySkill()
    {
        WeaponSlot w = CurrentHardHighlighted;
        if (stats.hp >= w.cost && !weps.ContainsWeapon(w.weapon.Name))
        {
            stats.hp -= w.cost;
            weps.AddWeapon(w);
            sound.Play();
        }
    }


    private void Update()
    {
        if(Vector2.Distance(transform.position, stats.transform.position) <= rangeOfBuy)
        {
            interact.gameObject.SetActive(true);

            if (Input.GetKeyDown(KeyCode.E))
            {
                stats.GetComponent<Movement>().enabled = false;
                menu.gameObject.SetActive(true);
            }
        }
        else
        {
            interact.gameObject.SetActive(false);
        }
    }

    public void CloseMenu()
    {
        stats.GetComponent<Movement>().enabled = true;
        menu.gameObject.SetActive(false);
    }
}
