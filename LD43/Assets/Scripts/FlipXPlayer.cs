﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipXPlayer : MonoBehaviour {

    private SpriteRenderer sprite;

    public bool fliped
    {
        get
        {
            return sprite.flipX;
        }
    }

    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (Input.GetAxisRaw("Horizontal") == -1)
        {
            sprite.flipX = true;
        }
        else if(Input.GetAxisRaw("Horizontal") == 1)
        {
            sprite.flipX = false;
        }
    }
}
