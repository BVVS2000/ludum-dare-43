﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Skill : MonoBehaviour {

    public float cost;

    public abstract void Use();

    public abstract bool Condition();
}
