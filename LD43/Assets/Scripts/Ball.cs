﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Ball : MonoBehaviour {

    Vector2 direction;
    float speed;
    public float dmg;

    Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        rb.MovePosition(transform.position + (Vector3)direction * speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(!collision.collider.CompareTag("Player"))
        {
            Destroy(gameObject);
        }
    }

    public void Push(Vector2 direction, float speed, float dmg)
    {
        this.direction = direction;
        this.speed = speed;
        this.dmg = dmg;
    }
}
