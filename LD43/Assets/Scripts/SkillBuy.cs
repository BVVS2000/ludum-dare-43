﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class SkillBuy : MonoBehaviour {

    PlayerStats stats;
    Skiller skiller;
    public float rangeOfBuy = 1f;
    public Image interact;
    public Image menu;

    public PlaySound sound;

    private void Start()
    {
        stats = FindObjectOfType<PlayerStats>();
        skiller = FindObjectOfType<Skiller>();
    }

    private SkillSlot CurrentHardHighlighted
    {
        get
        {
            return FindObjectsOfType<SkillSlot>().Where(s => s.hardHighlighted).First();
        }
    }

    public void BuySkill()
    {
        SkillSlot s = CurrentHardHighlighted;
        if(stats.condition >= s.cost && !skiller.ContainsSkill(s.skill) && skiller.transform.childCount<4)
        {
            stats.condition -= s.cost;
            Instantiate(s.skill.gameObject, skiller.transform);
            skiller.RefreshSkills();
            skiller.AddSkill(s.icon);
            sound.Play();
        }
    }

    private void Update()
    {
        if (Vector2.Distance(transform.position, stats.transform.position) <= rangeOfBuy)
        {
            interact.gameObject.SetActive(true);

            if (Input.GetKeyDown(KeyCode.E))
            {
                stats.GetComponent<Movement>().enabled = false;
                menu.gameObject.SetActive(true);
            }
        }
        else
        {
            interact.gameObject.SetActive(false);
        }
    }

    public void CloseMenu()
    {
        stats.GetComponent<Movement>().enabled = true;
        menu.gameObject.SetActive(false);
    }
}
