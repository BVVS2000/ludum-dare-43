﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class HighlightedWeaponDescription : MonoBehaviour {

    public Text title;
    public Text description;
    public Image icon;

    private WeaponSlot CurrentHighlighted
    {
        get
        {
            return FindObjectsOfType<WeaponSlot>().Where(s => s.highlighted).FirstOrDefault();
        }
    }

    private WeaponSlot CurrentHardHighlighted
    {
        get
        {
            return FindObjectsOfType<WeaponSlot>().Where(s => s.hardHighlighted).First();
        }
    }


    private void Update()
    {
        WeaponSlot s = CurrentHighlighted;

        if(!s)
        {
            s = CurrentHardHighlighted;
        }

        title.text = s.name;
        description.text = "Description:\nDMG: " + s.weapon.dmg + "\nRange" + s.weapon.atackDistance + "\nAtack Delay: " + s.weapon.cooldown + "\n\nPrize: " + s.cost + " CP";
        icon.sprite = s.icon;
    }
}
