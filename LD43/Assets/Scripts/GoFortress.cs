﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GoFortress : MonoBehaviour {
    
    public float rangeOfBuy = 1f;
    public Image interact;
    Transform player;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        if (Vector2.Distance(transform.position, player.position) <= rangeOfBuy)
        {
            interact.gameObject.SetActive(true);

            if (Input.GetKeyDown(KeyCode.E))
            {
                SceneManager.LoadScene("Las");
            }
        }
        else
        {
            interact.gameObject.SetActive(false);
        }
    }
}
