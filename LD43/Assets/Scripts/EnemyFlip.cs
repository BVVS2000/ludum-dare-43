﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFlip : MonoBehaviour {

    public SpriteRenderer body;
    public SpriteRenderer hand;
    public Transform pivot;

    public Animator anim;

    Vector2 defaultPos;
    public Vector2 newPos;

    Transform player;

    public bool fliped
    {
        get
        {
            return body.flipX;
        }
    }

    private void Start()
    {
        defaultPos = pivot.localPosition;
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        if((player.position - transform.position).x < 0)
        {
            UnFlip();
        }
        else if((player.position - transform.position).x > 0)
        {
            Flip();
        }

        anim.SetBool("fliped", fliped);

        if (pivot)
        {
            if (fliped)
            {
                pivot.localPosition = newPos;
            }
            else
            {
                pivot.localPosition = defaultPos;
            }
        }
    }

    public void Flip()
    {
        body.flipX = true;
        if(hand) hand.flipX = true;
    }

    public void UnFlip()
    {
        body.flipX = false;
        if (hand) hand.flipX = false;
    }
}
