﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Atack : MonoBehaviour {

    public float dmg = 10f;
    public float atackDistance = 1f;
    [SerializeField] private float angleOfAtack = 90f;

    public float cooldown = 1f;
    private float toNextAtack = 0.0f;

    public Animator anim;
    private Rulles rulles;
    private FlipXPlayer flip;

    public PlaySound audio;

    public bool superatack = false;

    private float angle;

    private void Start()
    {
        angle = angleOfAtack / 2;
        rulles = FindObjectOfType<Rulles>();
        flip = FindObjectOfType<FlipXPlayer>();
    }

    public string Name;

    private void Update()
    {
        rulles = FindObjectOfType<Rulles>();
        if (Input.GetButtonDown("Fire1") && toNextAtack<=0.0f && rulles.canAtack)
        {
            List<GameObject> allEnemies = new List<GameObject>(GameObject.FindGameObjectsWithTag("Enemy"));
            List<GameObject> tempEnemies = new List<GameObject>();
            List<GameObject> enemies = new List<GameObject>();

            if(GameObject.FindGameObjectWithTag("boss"))
            {
                allEnemies.Add(GameObject.FindGameObjectWithTag("boss"));
            }

            foreach (GameObject obj in allEnemies)
            {
                if(Vector2.Distance(obj.transform.position, transform.position) <= atackDistance * (superatack ? 1.2 : 1))
                {
                    tempEnemies.Add(obj);
                }
            }

            foreach(GameObject obj in tempEnemies)
            {
                if (Angle(obj) >= 90-angle && Angle(obj) <= 90+angle)
                {
                    enemies.Add(obj);
                }
            }

            foreach(GameObject en in enemies)
            {
                if (en.tag == "Enemy")
                {
                    if (en.GetComponent<Enemy>())
                    {
                        en.GetComponent<Enemy>().GetDamage(dmg * (superatack ? 1.5f : 1f));
                        audio.Play();
                    }
                    else
                    {
                        en.GetComponent<WiedzmaEnemy>().GetDamage(dmg * (superatack ? 1.5f : 1f));
                        audio.Play();
                    }
                }
                else
                {
                    en.GetComponent<WiedzmaEnemy>().GetDamage(dmg * (superatack ? 1.5f : 1f));
                    audio.Play();
                }

            }

            if(superatack)
            {
                superatack = false;
            }

            toNextAtack = cooldown;
            anim.SetTrigger("Atack");
        }
        else if(toNextAtack>0.0f)
        {
            toNextAtack -= Time.deltaTime;
        }
    }

    float Angle(GameObject obj)
    {
        if(flip.fliped)
        {
            Vector2 dir = obj.transform.position - GameObject.FindGameObjectWithTag("Player").transform.position;
            dir.Normalize();
            return -Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
        }
        else
        {
            Vector2 dir = obj.transform.position - GameObject.FindGameObjectWithTag("Player").transform.position;
            dir.Normalize();
            return Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
        }
    }
}
