﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fade : MonoBehaviour {

    public float onVision = 10f;

    public Image img;

    private void Update()
    {
        if (onVision <= 0)
        {
            img.gameObject.SetActive(false);
        }
        else
        {
            onVision -= Time.deltaTime;
        }
    }
}
