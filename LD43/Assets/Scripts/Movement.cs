﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Movement : MonoBehaviour {

    [SerializeField] private float speed = 10f;

    private Rigidbody2D rb;
    private Animator animator;
    Rulles rulles;

    private float inputX = 0f, inputY = 0f;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = transform.GetComponentInChildren<Animator>();
        rulles = FindObjectOfType<Rulles>();
    }

    public bool isMoving
    {
        get
        {
            return (inputX != 0 || inputY != 0) && rulles.canMove;
        }
    }

    private void Update()
    {
        rulles = FindObjectOfType<Rulles>();

        inputX = Input.GetAxisRaw("Horizontal");
        inputY = Input.GetAxisRaw("Vertical");

        if (isMoving)
        {
            animator.SetBool("isMoving", true);
        }
        else 
        {
            animator.SetBool("isMoving", false);
        }
    }

    private void FixedUpdate()
    {
        if (rulles.canMove)
        {
            Vector2 move = new Vector2(inputX, inputY) * speed * Time.deltaTime;

            rb.MovePosition((Vector2)transform.position + move);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Dash d = FindObjectOfType<Dash>();
        if(d)
        {
            d.StopDash();
        }
    }
}
