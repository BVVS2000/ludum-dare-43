﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WiedzmaAtack : MonoBehaviour
{
    public float prepareTime;
    public float armedTime;
    public float dmg;
    public float delay;
    public float atackRange;
    public float distanceToStartAtack;

    public bool isAtacking = false;
    public bool armed = false;

    private float toAtack = 0.0f;
    private float toEndPrepare = 0.0f;
    private float toEndBeArmed = 0.0f;

    Transform player;
    public Animator animator;
    public Ball ball;
    public Transform spawnPoint;
    float ballSpeed = 1.5f;
    Rulles rulles;

    public PlaySound sound;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        rulles = FindObjectOfType<Rulles>();
    }

    private void Update()
    {
        if (gameObject.tag == "Enemy" || (gameObject.tag == "boss" && rulles.bossCanDoAnything))
        {
            animator.SetBool("isAtacking", isAtacking);

            if (Vector2.Distance(player.position, transform.position) <= distanceToStartAtack && toAtack <= 0.0f && !isAtacking)
            {
                Atack();
            }
            else if (toAtack > 0.0f && !isAtacking)
            {
                toAtack -= Time.deltaTime;
            }

            if (isAtacking && !armed)
            {
                toEndPrepare -= Time.deltaTime;

                if (toEndPrepare <= 0.0f)
                {
                    armed = true;
                    toEndBeArmed = armedTime;
                }
            }

            if (isAtacking && armed)
            {
                Ball b = Instantiate(ball.gameObject, spawnPoint.position, Quaternion.identity).GetComponent<Ball>();
                b.Push(player.position - spawnPoint.position, ballSpeed, dmg);

                armed = isAtacking = false;
                sound.Play();
            }
        }
    }

    void Atack()
    {
        toEndPrepare = prepareTime;
        isAtacking = true;
        toAtack = delay;
    }
}
