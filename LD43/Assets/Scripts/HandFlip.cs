﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandFlip : MonoBehaviour {

    public SpriteRenderer hand1;
    public SpriteRenderer hand2;

    Vector2 defaultHand1;
    Vector2 defaultHand2;

    public Vector2 newPosHand1;
    public Vector2 newPosHand2;

    private void Start()
    {
        defaultHand1 = hand1.transform.localPosition;
        defaultHand2 = hand2.transform.localPosition;
    }

    private void Update()
    {
        if (Input.GetAxisRaw("Horizontal") == -1)
        {
            if (hand1)
            {
                hand1.sortingOrder = -1;
                hand1.flipX = true;
                hand1.transform.localPosition = newPosHand1;
            }

            if (hand2)
            {
                hand2.sortingOrder = 1;
                hand2.flipX = true;
                hand2.transform.localPosition = newPosHand2;
            }
        }
        else if (Input.GetAxisRaw("Horizontal") == 1)
        {
            if (hand1)
            {
                hand1.sortingOrder = 1;
                hand1.flipX = false;
                hand1.transform.localPosition = defaultHand1;
            }

            if (hand2)
            {
                hand2.sortingOrder = -1;
                hand2.flipX = false;
                hand2.transform.localPosition = defaultHand2;
            }
        }
    }
}
